package server

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

func precallback(file *os.File) {

}

func postcallback(file *os.File) {
}

func GethashandMove(id, hashes string) (string, error) {
	uploadFile := files[id]
	precallback(uploadFile.file)
	fdata, err := ioutil.ReadFile(files[id].tempPath)
	checkError(err)

	var returnMD5String string
	if hashes != "" {
		returnMD5String, err = checkhashes(hashes, fdata)
		if err != nil {
			//todo error fix
			return "", err
		}
	} else {
		h := md5.New()
		h.Write(fdata)
		hashInBytes := h.Sum(nil)
		returnMD5String = hex.EncodeToString(hashInBytes)
	}

	filePath := FileStorage.Path + "/" + returnMD5String[:2] + "/" + returnMD5String
	if _, err := os.Stat(filePath); !os.IsNotExist(err) {
		/*//todo file exist
		t := time.Now().Format(time.RFC3339)
		filePath = FileStorage.Path + "/" + t + "-" + uploadFile.name*/
	} else {
		err = os.MkdirAll(FileStorage.Path+"/"+returnMD5String[:2]+"/", os.ModePerm)
		checkError(err)
	}
	uploadFile.file.Close()
	err = MoveFile(uploadFile.tempPath, filePath)
	//checkError(err)
	delete(files, id)
	return returnMD5String, nil
}

func MoveFile(sourcePath, destPath string) error {
	inputFile, err := os.Open(sourcePath)
	if err != nil {
		return fmt.Errorf("Couldn't open source file: %s", err)
	}
	outputFile, err := os.Create(destPath)
	if err != nil {
		inputFile.Close()
		return fmt.Errorf("Couldn't open dest file: %s", err)
	}
	defer outputFile.Close()
	_, err = io.Copy(outputFile, inputFile)
	inputFile.Close()
	if err != nil {
		return fmt.Errorf("Writing to output file failed: %s", err)
	}
	// The copy was successful, so now delete the original file
	err = os.Remove(sourcePath)
	if err != nil {
		return fmt.Errorf("Failed removing original file: %s", err)
	}
	postcallback(outputFile)
	return nil
}

func checkError(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func renderAnswer(w http.ResponseWriter, message string, err error, statusCode int) {
	log.Printf(message+" %v", err)
	w.WriteHeader(statusCode)
	w.Write([]byte(message))
}

func parseContentRange(contentRange string) (totalSize int64, partFrom int64, partTo int64) {
	contentRange = strings.Replace(contentRange, "bytes ", "", -1)
	fromTo := strings.Split(contentRange, "/")[0]
	totalSize, err := strconv.ParseInt(strings.Split(contentRange, "/")[1], 10, 64)
	checkError(err)

	splitted := strings.Split(fromTo, "-")

	partFrom, err = strconv.ParseInt(splitted[0], 10, 64)
	checkError(err)
	partTo, err = strconv.ParseInt(splitted[1], 10, 64)
	checkError(err)

	return totalSize, partFrom, partTo
}
