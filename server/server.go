package server

import (
	"context"
	"crypto/md5"
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-redis/redis"
	"hash"
	"io"
	"io/ioutil"
	"mime"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

// map for save uploadFile while processing
type uploadFile struct {
	file       *os.File
	name       string
	tempPath   string
	status     string
	size       int64
	transfered int64
}

var files = make(map[string]*uploadFile)

// FileStorage settings. When finished uploading with success files are stored inside Path config. While uploading temporary files are stored inside TempPath directory.
type fileStorage struct {
	Path     string
	TempPath string
}

var FileStorage = fileStorage{
	Path:     ".store",
	TempPath: ".tmp",
}

//work with redis
var redisClient *redis.Client

type RedisCashValue struct {
	Name       string
	Size       int64
	DateUpload int64
	DateDelete int64
	DownCount  int
}

var ops, refreshbytes, notusedlimit int64

func init() {

	redisClient = redis.NewClient(&redis.Options{
		Network:  "tcp",
		Addr:     "db:6379",
		Password: "",
		DB:       0,
	})
	_, err := redisClient.Ping().Result()
	if err != nil {
		fmt.Printf("Reis cant start error: %v", err)
		return
	}

	refreshbytes = 10000000
	notusedlimit = 2
}

// UploadFile is main request/response handler for HTTP server.
func UploadFile(w http.ResponseWriter, r *http.Request) {
	if _, err := os.Stat(FileStorage.Path); os.IsNotExist(err) {
		os.MkdirAll(FileStorage.Path, os.ModePerm)
	}
	if _, err := os.Stat(FileStorage.TempPath); os.IsNotExist(err) {
		os.MkdirAll(FileStorage.TempPath, os.ModePerm)
	}

	if r.Method != "POST" || r.Header.Get("Session-ID") == "" || r.Header.Get("Content-Range") == "" {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Invalid request."))
	}

	sessionID := r.Header.Get("Session-ID")
	contentRange := r.Header.Get("Content-Range")

	body, err := ioutil.ReadAll(r.Body)
	checkError(err)

	totalSize, partFrom, partTo := parseContentRange(contentRange)

	if partFrom == 0 {
		_, ok := files[sessionID]
		if !ok {
			w.WriteHeader(http.StatusCreated)

			_, params, err := mime.ParseMediaType(r.Header.Get("Content-Disposition"))
			checkError(err)
			fileName := params["filename"]

			newFile := FileStorage.TempPath + "/" + sessionID
			_, err = os.Create(newFile)
			checkError(err)

			f, err := os.OpenFile(newFile, os.O_APPEND|os.O_RDWR, os.ModeAppend)
			checkError(err)

			files[sessionID] = &uploadFile{
				file:     f,
				name:     fileName,
				tempPath: newFile,
				status:   "created",
				size:     totalSize,
			}
		}
	} else {
		w.WriteHeader(http.StatusOK)
	}

	upload, ok := files[sessionID]
	if ok {
		upload.status = "uploading"
		_, err = upload.file.Write(body)
		checkError(err)

		err = upload.file.Sync()
		checkError(err)

		upload.transfered = partTo
		w.Header().Set("Content-Length", string(len(body)))
		w.Header().Set("Connection", "close")
		w.Header().Set("Range", contentRange)
		w.Write([]byte(contentRange))

		if partTo >= totalSize {
			rthash, err := GethashandMove(sessionID, r.Header.Get("hashes"))
			checkError(err)
			renderAnswer(w, rthash, nil, http.StatusOK)
			redbyte, err := json.Marshal(&RedisCashValue{upload.name, upload.size, time.Now().Unix(), 0, 0})
			if err != nil {
				fmt.Println("Error redis marshal " + err.Error())
			}
			err = redisClient.Set(rthash, string(redbyte), 0).Err()
			if err != nil {
				fmt.Println(err)
			}
		}
	} else {
		//todo error
	}
}

func DownloadFile(w http.ResponseWriter, r *http.Request) {

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		renderAnswer(w, "failed to read body", err, http.StatusBadRequest)
		return
	}

	MD5String := string(data)
	if strings.Contains(MD5String, ":") {
		MD5String = MD5String[strings.Index(MD5String, ":")+1:]
	}

	filePath := FileStorage.Path + "/" + MD5String[:2] + "/" + MD5String
	if _, err := os.Stat(filePath); !os.IsNotExist(err) {
		//exist
		fdata, err := ioutil.ReadFile(filePath)
		checkError(err)
		w.Write(fdata)
		returnValue := RedisCashValue{}
		answer, err := redisClient.Get(MD5String).Result()
		if err != nil {
			fmt.Println("Error redis get " + err.Error())
		}
		err = json.Unmarshal([]byte(answer), &returnValue)
		if err != nil {
			fmt.Println("Error unmarshal redis struct " + err.Error())
		}
		redbyte, err := json.Marshal(&RedisCashValue{returnValue.Name, returnValue.Size, returnValue.DateUpload, 0, returnValue.DownCount + 1})
		if err != nil {
			fmt.Println("Error redis marshal " + err.Error())
		}
		err = redisClient.Set(MD5String, string(redbyte), 0).Err()
		if err != nil {
			fmt.Println(err)
		}
		return
	} else {
		renderAnswer(w, "not exist", nil, http.StatusBadRequest)
		return
	}
}

func DeleteFile(w http.ResponseWriter, r *http.Request) {

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		renderAnswer(w, "failed to read body", err, http.StatusBadRequest)
		return
	}

	MD5String := string(data)
	if strings.Contains(MD5String, ":") {
		MD5String = MD5String[strings.Index(MD5String, ":")+1:]
	}

	filePath := FileStorage.Path + "/" + MD5String[:2] + "/" + MD5String
	if _, err := os.Stat(filePath); !os.IsNotExist(err) {
		//exist
		err = os.Remove(filePath)
		checkError(err)

		f, err := os.Open(FileStorage.Path + "/" + MD5String[:2] + "/")
		if err != nil {
			//todo error
		}
		defer f.Close()
		_, err = f.Readdirnames(1) // Or f.Readdir(1)
		if err == io.EOF {
			err = os.Remove(FileStorage.Path + "/" + MD5String[:2] + "/")
			checkError(err)
		}
		renderAnswer(w, "file deleted", nil, http.StatusOK)
		returnValue := RedisCashValue{}
		answer, err := redisClient.Get(MD5String).Result()
		if err != nil {
			fmt.Println("Error redis get " + err.Error())
		}
		err = json.Unmarshal([]byte(answer), &returnValue)
		if err != nil {
			fmt.Println("Error unmarshal redis struct " + err.Error())
		}
		redbyte, err := json.Marshal(&RedisCashValue{returnValue.Name, returnValue.Size, returnValue.DateUpload, time.Now().Unix(), returnValue.DownCount})
		if err != nil {
			fmt.Println("Error redis marshal " + err.Error())
		}
		err = redisClient.Set(MD5String, string(redbyte), 0).Err()
		if err != nil {
			fmt.Println(err)
		}
		return
	} else {
		renderAnswer(w, "not exist", nil, http.StatusBadRequest)
		return
	}
}

func Close() {
	redisClient.Close()
	return
}

//контроль целостности файлов
func checkhashes(hashes string, filebuf []byte) (string, error) {
	var wg sync.WaitGroup

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel() // Make sure it's called to release resources even if no errors

	slchashes := strings.Split(hashes, ",")
	errs := make(chan error, len(slchashes))
	result := make(chan string, 1)
	for _, ourhash := range slchashes {
		wg.Add(1)

		var hashtype hash.Hash
		myhash := strings.Split(ourhash, ":")

		if strings.Contains(ourhash, ":") {
			ourhash = ourhash[strings.Index(ourhash, ":")+1:]
		} else {
			//todo error if wronf format
			continue
		}

		switch namehash := myhash[0]; namehash {
		case "md5":
			hashtype = md5.New()
		case "sha1":
			hashtype = sha1.New()
		default:
			fmt.Printf("default")
		}

		go func(ourhash hash.Hash, buf []byte, value string, namehash string) {
			defer wg.Done()

			// Check if any error occurred in any other gorouties:
			select {
			case <-ctx.Done():
				return // Error somewhere, terminate
			default: // Default is must to avoid blocking
			}

			ourhash.Write(buf)
			recievedhash := ourhash.Sum(nil)
			strHash := hex.EncodeToString(recievedhash)
			if strHash != value {
				errs <- fmt.Errorf("Stop routine error: ", errors.New("Hash wrong: "+strHash))
				cancel()
				return
			}
			if namehash == "md5" {
				result <- strHash
			}
			fmt.Printf("Hash %v is good", namehash)

		}(hashtype, filebuf, myhash[1], myhash[0])
	}
	wg.Wait()
	if ctx.Err() != nil {
		return "", <-errs
	}
	res := <-result
	return res, nil
}
