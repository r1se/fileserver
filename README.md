dependencies:

go get github.com/go-redis/redis

go get github.com/ulule/limiter

go get github.com/ulule/limiter/drivers/store/redis

go get github.com/ulule/limiter/drivers/middleware/stdlib

go get gopkg.in/cheggaaa/pb.v1



server usage:

go run main.go --server


client usage:

go run main.go --client --action=upload --file="/test.txt" --hashes=md5:f7f63f9900be4e0c6f397f6b62b7724c,sha1:7695f9394e8a753350e895baaec8dda91e2a2c5d

go run main.go --client --action=download --hashes=md5:f7f63f9900be4e0c6f397f6b62b7724c

go run main.go --client --action=delete --hashes=md5:f7f63f9900be4e0c6f397f6b62b7724c


docker server start:

docker-compose up




