package main

import (
	"flag"
	"fmt"
	"github.com/go-redis/redis"
	"github.com/ulule/limiter"
	"github.com/ulule/limiter/drivers/middleware/stdlib"
	sredis "github.com/ulule/limiter/drivers/store/redis"
	"gitlab.com/r1se/fileserver/client"
	"gitlab.com/r1se/fileserver/server"
	"gopkg.in/cheggaaa/pb.v1"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

func main() {
	var url string
	//parse arguments
	isServer := flag.Bool("server", false, "")
	isClient := flag.Bool("client", false, "")
	filePath := flag.String("file", "./test.txt", "file to upload")
	action := flag.String("action", "upload", "client action")
	hashes := flag.String("hashes", "", "hashes=md5:f7f63f9900be4e0c6f397f6b62b7724c")
	flag.Parse()

	if *isServer {
		//Start redis
		redisClient := redis.NewClient(&redis.Options{
			Network:  "tcp",
			Addr:     "db:6379",
			Password: "",
			DB:       0,
		})
		_, err := redisClient.Ping().Result()
		if err != nil {
			fmt.Printf("Redis can't start error: %v", err)
			return
		}

		//Attach middleware to Redis
		store, err := sredis.NewStoreWithOptions(redisClient, limiter.StoreOptions{
			Prefix:   "limiter_http_example",
			MaxRetry: 3,
		})
		if err != nil {
			fmt.Printf("Limiter can't attach to Redis %v", err)
			return
		}

		// Create a new middleware with the limiter instance
		rate := limiter.Rate{
			Period: 1 * time.Hour,
			Limit:  1000,
		}
		middleware := stdlib.NewMiddleware(limiter.New(store, rate), stdlib.WithForwardHeader(true))

		http.Handle("/upload", middleware.Handler(http.HandlerFunc(server.UploadFile)))
		http.Handle("/download", middleware.Handler(http.HandlerFunc(server.DownloadFile)))
		http.Handle("/delete", middleware.Handler(http.HandlerFunc(server.DeleteFile)))
		fmt.Println("Listening on http://localhost:2110")
		http.ListenAndServe(":2110", nil)
	}

	if *isClient {
		//check client action
		if *action == "upload" {
			url = "http://localhost:2110/upload"
			httpClient := &http.Client{}
			chunkSize := int(1 * (1 << 20)) // 1MB

			//make new client for sending filepath and hashes
			newclient := client.New(url, *filePath, *hashes, httpClient, chunkSize, false)
			newclient.Init()

			//make prograss bar in console
			count := newclient.Status.Size
			bar := pb.New(int(count))
			bar.SetRefreshRate(time.Millisecond)
			bar.SetUnits(pb.U_BYTES)

			//start progress bar on routine
			go func() {
				bar.Start()
				for {
					newclient.Status.Rwmx.Lock()
					if newclient.Status.SizeTransferred > newclient.Status.Size {
						bar.FinishPrint("Done.")
						break
					}
					bar.Set(int(newclient.Status.SizeTransferred))
					newclient.Status.Rwmx.Unlock()
					time.Sleep(time.Millisecond)
				}
			}()

			//Start sending
			newclient.Start()
			client.WG.Wait()

		} else {
			if *action == "download" {
				url = "http://localhost:2110/download"
			} else if *action == "delete" {
				url = "http://localhost:2110/delete"
			} else {
				fmt.Printf("Wrong actions: use upload/download/delete")
				return
			}
			request, err := http.NewRequest("POST", url, strings.NewReader(*hashes))
			if err != nil {
				fmt.Printf("Can't make new requests %v", err)
				return
			}
			response, err := http.DefaultClient.Do(request)
			if err != nil {
				fmt.Printf("CAn't make http client %v", err)
				return
			}
			defer response.Body.Close()

			body, err := ioutil.ReadAll(response.Body)
			if err != nil {
				fmt.Printf("Can't read response bode %v", err)
				return
			}
			fmt.Println(string(body))
			return
		}
	}
}
