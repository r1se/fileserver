FROM golang:latest
RUN mkdir -p gitlab.com/r1se/fileserver
COPY . /go/src/gitlab.com/r1se/fileserver
WORKDIR /go/src/gitlab.com/r1se/fileserver

RUN go get github.com/go-redis/redis
RUN go get github.com/ulule/limiter
RUN go get github.com/ulule/limiter/drivers/store/redis
RUN go get github.com/ulule/limiter/drivers/middleware/stdlib
RUN go get gopkg.in/cheggaaa/pb.v1

RUN go build
CMD ["go","run","main.go","--server"]
EXPOSE 2110
